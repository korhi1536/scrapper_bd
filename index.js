const axios = require('axios');
const cheerio = require("cheerio");
const urls = require('./urls.json');

let promises = urls.map(url => axios.get(url));

var performRating = (result) => {
  const $ = cheerio.load(result.data);

  var ratingSpan = $('span.rating');
  var rating = 0;
  ratingSpan.each((i,r) => rating += parseFloat(r.firstChild.data));
  
  var comments = 0;
  $('span.count-rating').each((i, r) => comments += parseInt(r.firstChild.data.slice(1, -1).replace('<', '')));
  
  var quotes = 0;
  $('a.count-quotes').each((i, r) => quotes += parseInt(r.children[1].firstChild.data));

  let url =  result.config.url.split('/');

  return {
    avgRating: rating/ratingSpan.length,
    name: url[url.length - 1].replace(/.*knigi_/gi, ''),
    avgCommentsCount: comments/ratingSpan.length/5,
    avgQuotesCount: quotes/ratingSpan.length,
  };
};

Promise.all(promises).then(res => {
  let arr = [];
  let maxLength = 0;
  let maxRatingBar = 0;
  let maxCommentBar = 0;
  let maxQuotesBar = 0;
  for (let i = 0; i < res.length; i++) {
    arr.push(performRating(res[i]));

    if (arr[i].name.length > maxLength)
      maxLength = arr[i].name.length;

    if (arr[i].avgRating > maxRatingBar)
      maxRatingBar = arr[i].avgRating;

    if (arr[i].avgCommentsCount > maxCommentBar)
      maxCommentBar = arr[i].avgCommentsCount;

    if (arr[i].avgQuotesCount > maxQuotesBar)
      maxQuotesBar = arr[i].avgQuotesCount;
  }

  arr.sort((a1, a2) => Math.sign(a2.avgRating - a1.avgRating));
  console.log("Avg. rating by genre :\n")
  arr.forEach(pos => {
    let strong = Math.floor(pos.avgRating*10);
    let tailString = pos.avgRating*10 - strong > 2 / 3 ? '▓' : pos.avgRating*10 - strong > 1 / 3 ? '▒' : pos.avgRating*10 - strong > 0 ? '░' : '';
    let bar = `${'█'.repeat(strong)}${tailString}`.padEnd(maxRatingBar*10 + 5);
    let str = `${pos.name.padEnd(maxLength + 5)} ${bar} ${(pos.avgRating*10).toFixed(2)} / 100`;
    console.log(str);
  });

  arr.sort((a1, a2) => Math.sign(a2.avgCommentsCount - a1.avgCommentsCount));
  console.log("\nAvg. comments count by genre :\n")
  arr.forEach(pos => {
    let strong = Math.floor(pos.avgCommentsCount);
    let tailString = pos.avgCommentsCount*5 - Math.floor(pos.avgCommentsCount*5) > 2 / 3 ? '▓' : pos.avgCommentsCount*5 - Math.floor(pos.avgCommentsCount*5) > 1 / 3 ? '▒' : pos.avgCommentsCount*5 - Math.floor(pos.avgCommentsCount*5) > 0 ? '░' : '';
    let bar = `${'█'.repeat(strong)}${tailString}`.padEnd(maxCommentBar + 5);
    let str = `${pos.name.padEnd(maxLength + 5)} ${bar} ${(pos.avgCommentsCount*5).toFixed(2)}`;
    console.log(str);
  });

  arr.sort((a1, a2) => Math.sign(a2.avgQuotesCount - a1.avgQuotesCount));
  console.log("\nAvg. quotes count by genre :\n")
  arr.forEach(pos => {
    let strong = Math.floor(pos.avgQuotesCount);
    let tailString = pos.avgQuotesCount - strong > 2 / 3 ? '▓' : pos.avgQuotesCount - strong > 1 / 3 ? '▒' : pos.avgQuotesCount - strong > 0 ? '░' : '';
    let bar = `${'█'.repeat(strong)}${tailString}`.padEnd(maxQuotesBar + 5);
    let str = `${pos.name.padEnd(maxLength + 5)} ${bar} ${(pos.avgQuotesCount).toFixed(2)}`;
    console.log(str);
  });
});